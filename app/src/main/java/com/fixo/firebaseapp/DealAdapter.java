package com.fixo.firebaseapp;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DealAdapter extends RecyclerView.Adapter<DealAdapter.DealViewHolder
        >
{

    public static FirebaseDatabase mFirebaseDatabase;
    public static DatabaseReference mDatabaseReference;
    public static ArrayList<TravelDeal> deals;
    private ImageView imageDeal;

    ChildEventListener mChildListener;
    public DealAdapter()
    {
       // initializing the firebase dabase and database reference
        mFirebaseDatabase = FirebaseUtil.mFirebaseDatabase;
        mDatabaseReference = FirebaseUtil.mDatabaseReference;
        deals= FirebaseUtil.mDeals;

        //initializing the childEventListener
        mChildListener=new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //when a child is added to the database, we add it to the deals
                TravelDeal td=dataSnapshot.getValue(TravelDeal.class);
               td.setId(dataSnapshot.getKey());

               deals.add(td);
               notifyItemInserted(deals.size()-1);
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        //adding the childEventListener to the database reference
        mDatabaseReference.addChildEventListener(mChildListener);
    }

    @NonNull
    @Override
    public DealViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        //inflating the single item row layout to be displayed on the recyclerview
        Context context=parent.getContext();
        View view= LayoutInflater.from(context).inflate(R.layout.rv_row, parent, false);


        return new DealViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull DealViewHolder holder, int position) {

        TravelDeal deal=deals.get(position);

        //binding the deals to the holder
        holder.bind(deal);

    }

    @Override
    public int getItemCount() {
        return deals.size();
    }

    public class DealViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvTitle, tvPrice,tvDescription;

        public DealViewHolder(@NonNull View itemView) {
            super(itemView);

            //getting Ids of the views in the "rv_row" layout
            tvTitle=itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            imageDeal= itemView.findViewById(R.id.imageDeal);

            //setting onClikListener to the itemView
            itemView.setOnClickListener(this);
        }

        public void bind(TravelDeal deal)
        {
            //setting the tilte, price and description
            tvTitle.setText(deal.getTitle());
            tvDescription.setText(deal.getDescription());
            tvPrice.setText(deal.getPrice());
            showImage(deal.getImageUrl());
        }

        @Override
        public void onClick(View view) {
            int position= getAdapterPosition();

            //storing the clicked deal details to an intent then passind them to DealActivity
            TravelDeal selectedDeal= deals.get(position);
            Intent intent= new Intent(view.getContext(), DealActivity.class);
            intent.putExtra("Deal", selectedDeal);
            view.getContext().startActivity(intent);


        }
        private void showImage(String url)
        {
            /*checking if the image url is not null && empty then displaying the image from the url
            using Picasso library */

            if(url !=null && !url.isEmpty())
            {
                Picasso.with(imageDeal.getContext())
                        .load(url)
                        .resize(160, 160)
                        .centerCrop()
                        .into(imageDeal);
            }
        }

    }
}
