package com.fixo.firebaseapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


public class ListlActivity extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //inflating the options menu
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.list_activity_menu, menu);

        MenuItem saveMenuItem=menu.getItem(0);

        //checking if the user is admin. If he/she is one, the they are allow viw the save menu item

        if(FirebaseUtil.isAdmin==true){
            saveMenuItem.setVisible(true);
           // menu.findItem(R.id.insert_menu).setVisible(true);
        }
        else {
            saveMenuItem.setVisible(false);
            //menu.findItem(R.id.insert_menu).setVisible(false);

        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        //working with the selected options menu items
        switch (item.getItemId())
        {
            case R.id.insert_menu:
                startActivity(new Intent(ListlActivity.this, DealActivity.class));
                return true;
            case R.id.logout_menu:
                AuthUI.getInstance()
                        .signOut(this)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            public void onComplete(@NonNull Task<Void> task) {
                                FirebaseUtil.attachListener();
                                Toast.makeText(ListlActivity.this, "Your are logged out", Toast.LENGTH_LONG).show();
                            }
                        });
                FirebaseUtil.detachListener();
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        //initializing the firebase reference
        FirebaseUtil.openFBReference("traveldeals",this);

        //finding Id of the recycler view
        RecyclerView recyclerView = findViewById(R.id.rvDeals);

        //initializing the linear layout manager
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);

        //settiing the linear layout to the recycler view
        recyclerView.setLayoutManager(linearLayoutManager);

        //initializing the adapter
        final DealAdapter dealAdapter = new DealAdapter();

        //setting the adapter the recycler vew
        recyclerView.setAdapter(dealAdapter);
         recyclerView.setAdapter(dealAdapter);

         //calling method to attach firebase listener
        FirebaseUtil.attachListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //detaching the firbase listener
        FirebaseUtil.detachListener();
    }

    public void showMenu(){
        //hiding the options menu
        invalidateOptionsMenu();
    }
}
