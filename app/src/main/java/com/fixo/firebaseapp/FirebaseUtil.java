package com.fixo.firebaseapp;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FirebaseUtil {

    public static FirebaseDatabase mFirebaseDatabase;
    public static DatabaseReference mDatabaseReference;
    public static ArrayList<TravelDeal> mDeals;
    private static FirebaseUtil firebaseUtil;
    public static FirebaseAuth mFirebaseAuth;
    public static FirebaseAuth.AuthStateListener mAuthListener;
    private static ListlActivity caller;
    private static int RC_SIGN_IN=34;
    public static FirebaseStorage mFirebaseStorage;
    public static StorageReference mStorageReference;
    public static boolean isAdmin;

    private FirebaseUtil(){}

    public static void openFBReference(String ref, final ListlActivity callerActivity){

        if(firebaseUtil ==null){

            //iniatilizing firebase database and firebase authentication
            firebaseUtil = new FirebaseUtil();
            mFirebaseDatabase =FirebaseDatabase.getInstance();
            mFirebaseAuth= FirebaseAuth.getInstance();
            caller=callerActivity;

            //initializing firebase authenticationListener
            mAuthListener= new FirebaseAuth.AuthStateListener() {
                @Override
                public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                    /*getting the user currently logged in, if he/she is not logged in then
                    * call singIn method else check if he/she is an admin
                     */

                    if(firebaseAuth.getCurrentUser()==null)
                    {
                       FirebaseUtil.signIn();
                    }
                    else
                    {
                        String userId=mFirebaseAuth.getUid();
                        checkAdmin(userId);
                    }



                    Toast.makeText(callerActivity.getBaseContext(), "Welcome back", Toast.LENGTH_LONG).show();

                }
            };

            /*calling connectStorage() method to connect us to firebase storage so that we can
            save photos
             */

          connectStorage();

        }
        //initializing arraylist
        mDeals = new ArrayList<TravelDeal>();

        //setting the firebase database reference
        mDatabaseReference = mFirebaseDatabase.getReference().child(ref);
    }

    public static void attachListener()
    {
        //attaching the authentication listener
        mFirebaseAuth.addAuthStateListener(mAuthListener);
    }
    public static void detachListener()
    {
        //detaching the authentication listener
        mFirebaseAuth.removeAuthStateListener(mAuthListener);
    }

    private static void signIn()
    {
        // Choose authentication providers
        List<AuthUI.IdpConfig> providers = Arrays.asList(
                new AuthUI.IdpConfig.EmailBuilder().build(),
                new AuthUI.IdpConfig.GoogleBuilder().build()

        );



// Create and launch sign-in intent
        caller.startActivityForResult(
                AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .build(),
                RC_SIGN_IN);
    }

    private static void checkAdmin(String uId){

        FirebaseUtil.isAdmin=false;

        //setting databse reference to get the "administrators node from the firebase database
        DatabaseReference reference=mFirebaseDatabase.getReference().child("administrators").child(uId);

        //adding childEvent listener
        ChildEventListener childEventListener= new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                //since the user is from the "administrators node, we set the isAdmin boolean to true
                FirebaseUtil.isAdmin=true;

                //we show the options menu items that should be viewed by an admin
                caller.showMenu();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        //adding the database referece to the childEvenListener to listen to any changes
        reference.addChildEventListener(childEventListener);

    }
    private static void connectStorage(){
        //getting the instance of the firebase storage
        mFirebaseStorage=FirebaseStorage.getInstance();

        //initialiing the frebase storage reference to the firebase storage with the child "deals_pictures"
        mStorageReference=mFirebaseStorage.getReference().child("deals_pictures");
    }

}
