package com.fixo.firebaseapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

public class DealActivity extends AppCompatActivity {

    EditText txtTitle, txtDescription, txtPrice;
    public static FirebaseDatabase mFirebaseDatabase;
    public static DatabaseReference mDatabaseReference;
    //private DatabaseReference mDatabaseReference;

   // private EditText txtTitle,txtDescription, txtPrice;
    TravelDeal deal;
    private static final int  REQUEST_CODE=32;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trial);

        //finding Ids of views
        txtTitle=findViewById(R.id.title);
        txtDescription = findViewById(R.id.description);
        txtPrice = findViewById(R.id.price);
        imageView = findViewById(R.id.image);



        //initializing firebase database and databse reference
        mFirebaseDatabase = FirebaseUtil.mFirebaseDatabase;
        mDatabaseReference = FirebaseUtil.mDatabaseReference;

        //getting the data passed by intent from ListActivity
        Intent intent= getIntent();
        TravelDeal deal=(TravelDeal)intent.getSerializableExtra("Deal");


        if(deal==null){
            deal= new TravelDeal();
        }
        this.deal=deal;

        //setting the title, description and price
        txtTitle.setText(deal.getTitle());
        txtDescription.setText(deal.getDescription());
        txtPrice.setText(deal.getPrice());
        //finding Id of upload image button

        Button uploadPicture = findViewById(R.id.btnImage);

        //setting onclickListener
        uploadPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //starting new intent to read pictures from internal storage
                Intent intent= new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(intent,"insert picture"), REQUEST_CODE);
            }
        });

        //calli g showImage() method to display the image of a deal
        showImage(deal.getImageUrl());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==REQUEST_CODE && resultCode==RESULT_OK)
        {
            //gettig uri of the image data
            Uri imageUri=data.getData();

            assert imageUri != null;
            //getting the reference of firebase storage
            final StorageReference ref= FirebaseUtil.mStorageReference.child(imageUri.getLastPathSegment());

            // storing the image into firebase storage and adding a success listener
            ref.putFile(imageUri).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                    //getting the url of the image in the firebase storage
                    String url=ref.getDownloadUrl().toString();

                    //then setting the url using setImageUrl() setter method
                    deal.setImageUrl(url);

                    //getting the image name from fribase storage and setting the name using the setter method
                    String imageName=taskSnapshot.getStorage().getPath();
                    deal.setImageName(imageName);

                    //calling the showImage() method to display the image from the url
                    showImage(url);
                }
            });


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //inflating options menu
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.save_menu, menu);


        /*checking if a logged-in user is an admin
         *if one is and admin, then he/she is allowed to view the save and delete items in the
         * options menu else only the logout menu
         * an admin is also allow to use the "ulpoad image" button. for a normal user, the button
         * is disabled

         */
        if(FirebaseUtil.isAdmin)
        {
            menu.findItem(R.id.save).setVisible(true);
            menu.findItem(R.id.delete_deal).setVisible(true);
            enableEditTexts(true);

             findViewById(R.id.btnImage).setEnabled(true);

        }
        else
        {
            menu.findItem(R.id.save).setVisible(false);
            menu.findItem(R.id.delete_deal).setVisible(false);
           enableEditTexts(false);
           findViewById(R.id.btnImage).setEnabled(false);
        }



        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        //working with the items in the options menu
        switch (item.getItemId())
        {
            case R.id.save:
                saveDeals();
                Toast.makeText(this, "Deal saved", Toast.LENGTH_SHORT).show();
                clean();
                backToListActivity();
                return true;
            case R.id.delete_deal:
                deleteDeal();
                Toast.makeText(this, "Deal deleted", Toast.LENGTH_LONG).show();
                backToListActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }


    }

    private void  saveDeals(){

        //getting the title, descrption and price t
        deal.setTitle( txtTitle.getText().toString());
        deal.setDescription(txtDescription.getText().toString());
        deal.setPrice(txtPrice.getText().toString());

        //if deaal is null the we save it else we update it
        if(deal.getId()==null)
        {

            mDatabaseReference.push().setValue(deal);

        }
        else
        {
            mDatabaseReference.child(deal.getId()).setValue(deal);
        }


    }

    private void clean(){
        //setting empty strings the editTexts
        txtDescription.setText("");
        txtPrice.setText("");
        txtTitle.setText("");
        txtTitle.requestFocus();
    }

    /**
     * A method to delete a deal from firebase database and picture from storage
     *
     */

    private void deleteDeal() {
        //If a deal is not null, f it exists in the database, then it is deleted


            if (deal == null) {
                Toast.makeText(this, "Save the deal before deleting", Toast.LENGTH_LONG).show();
            } else {
                mDatabaseReference.child(deal.getId()).removeValue();
            }


            //we also delete the image associated with the deal from the firebase storage
            if (deal.getImageName() != null && !deal.getImageName().isEmpty()) {
                StorageReference pictureReference = FirebaseUtil.mFirebaseStorage.getReference().child(deal.getImageName());
                pictureReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(DealActivity.this, "Details deleted successfully", Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(DealActivity.this, "Ann error occurred while deleting picture", Toast.LENGTH_LONG).show();
                    }
                });
            }

    }

    private void backToListActivity(){
        startActivity(new Intent(this, ListlActivity.class));
    }


    private  void enableEditTexts(boolean isEnabled){

        /*for and admin, the editTexts are ebabled, else disabled by passsing false  to the method
        call */
        txtPrice.setEnabled(isEnabled);
        txtDescription.setEnabled(isEnabled);
        txtTitle.setEnabled(isEnabled);
    }

    private void showImage(String url){

        //check if the ur is not null and empty then display the image using Picasso library
        if(url != null && !url.isEmpty())
        {
            int width = Resources.getSystem().getDisplayMetrics().widthPixels;

            Picasso.with(this).
                    load(url)
                    .resize(width, width*2/3)
                    .centerCrop()
                    .into(imageView);
        }
    }
}
